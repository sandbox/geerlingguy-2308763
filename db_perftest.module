<?php

/**
 * @file
 * Database Performance Testing module.
 *
 * Note on memory usage measurement: I originally had an elaborate memory
 * measurement tool in place to try to estimate how many bytes were allocated
 * for different db access methods, but due to an extremely diverse array of
 * issues, memory measurement is not all that helpful (and when it's working,
 * it's often either slightly or wildly incorrect).
 */

/**
 * Simple db_query() test.
 */
function db_perftest_db_query() {
  return \Drupal::database()->query("SELECT nid FROM {node} WHERE type = 'page'")->fetchCol();
}

/**
 * Simple db_select() test.
 */
function db_perftest_db_select() {
  return \Drupal::database()->select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'page')
    ->execute()
    ->fetchCol();
}

/**
 * Simple EQ test.
 */
function db_perftest_eq() {
  $query = \Drupal::entityQuery('node');
  $query->condition('type', 'page');
  return $query->execute();
}

/**
 * More complex db_query() test with a join.
 */
function db_perftest_db_query_join() {
  return \Drupal::database()->query("SELECT n.nid
    FROM {node} n
    LEFT JOIN {node_field_revision} r ON r.nid = n.nid
    LEFT JOIN {users_field_data} u ON u.uid = r.uid
    WHERE n.type = 'page' AND u.status = 1")->fetchCol();
}

/**
 * More complex db_select() test with a join.
 */
function db_perftest_db_select_join() {
  $query = \Drupal::database()->select('node', 'n');
  $query->leftJoin('node_field_revision', 'r', 'r.nid = n.nid');
  $query->leftJoin('users_field_data', 'u', 'u.uid = r.uid');
  $query->fields('n', array('nid'))
    ->condition('n.type', 'page')
    ->condition('u.status', 1);
  return $query->execute()->fetchCol();
}
