<?php

namespace Drupal\db_perftest\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that measures performance of queries.
 */
class TestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'db_perftest';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = $this->getRunOptions();

    $form['tests_to_run'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Functions to test'),
      '#options' => $options,
      '#default_value' => array_keys($options),
      '#required' => TRUE,
    ];

    $form['test_run_count'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of test runs to perform'),
      '#description' => $this->t('One test run offers a fairly weak set of data.'),
      '#options' => [
        1 => '1',
        5 => '5',
        10 => '10',
        25 => '25',
        100 => '100',
      ],
      '#default_value' => 5,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $test_runs = [];
    $test_run_count = $form_state->getValue('test_run_count') + 1;
    $tests = array_filter($form_state->getValue('tests_to_run'));

    // TODO - Compile and average the results for multiple test runs!
    for ($i = $test_run_count; $i > 0; $i--) {
      $results = [];

      // Run each database function performance test and gather the results.
      foreach ($tests as $type => $label) {
        $result = [];
        $total_memory = 0;

        // Start timing.
        $time = microtime(TRUE);

        $function = 'db_perftest_' . $type;
        $function();

        $time = ((microtime(TRUE) - $time) * 1000);

        // End timing/memory usage collection.
        $result['type'] = $label;
        $result['time'] = $time;
        $results[] = $result;
      }

      // Add to the array of test runs.
      $test_runs[] = $results;
    }

    // Throw away the first result (could have memory inaccuracies).
    array_shift($test_runs);

    // Average all the test runs.
    $test_run_averages = $this->calculateAverage($test_runs);

    // Return the performance data using dpm.
    $this->messenger()->addMessage($this->t('Results for @count test runs', [
      '@count' => $test_run_count - 1,
    ]));
    foreach ($test_run_averages as $average) {
      $this->messenger()->addMessage($average);
    }
  }

  /**
   * Query options for use in selections and as function names.
   *
   * @return array
   *   Array of partial function names mapped to human-readable labels.
   */
  private function getRunOptions() {
    return [
      'db_query' => $this->t('query() Simple'),
      'db_select' => $this->t('select() Simple'),
      'eq' => $this->t('EntityQuery Simple'),
      'db_query_join' => $this->t('query() with Joins'),
      'db_select_join' => $this->t('select() with Joins'),
    ];
  }

  /**
   * Calculate averages over multiple test runs.
   *
   * @param array $runs
   *   Test run data, in an array.
   *
   * @return array
   *   Test run averages, keyed by type.
   */
  private function calculateAverage(array $runs) {
    $total = count($runs);
    $totals = [];
    $result = [];

    foreach ($runs as $run) {
      foreach ($run as $data) {
        $type = $data['type'];
        if (!isset($totals[$type])) {
          $totals[$type] = 0;
        }
        $totals[$type] += $data['time'];
      }
    }

    $options = $this->getRunOptions();
    foreach ($totals as $type => $time) {
      $result[$type] = $this->t('%type - @time ms', [
        '%type' => $options[$type],
        '@time' => round($time / $total, 6),
      ]);
    }

    return $result;
  }

}
